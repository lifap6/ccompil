# Programmation modulaire par fichiers en C / C++

## But de ce dépôt

Ce dépôt contient une séquence de commits réalisés lors d'une démonstration en
cours. Chaque commit illustre un point abordé.

## Utilisation de ce dépôt

Pour utiliser correctement ce dépôt, il est nécessaire d'utiliser `git` et de
savoir naviguer dans l'historique des commits. Pour débuter, vous pouvez
dupliquer le dépôt et le récupérer via la commande :

```
git clone https://forge.univ-lyon1.fr/lifap6/ccompil
```

Une fois le dépôt récupéré, il vous faut vous déplacer dans l'historique des
commits pour vous placer sur le commit souhaité. Pour lister les commits vous
pouvez utiliser

```
git log
```

voire pour les lister de façon plus concise

```
git log --pretty=oneline --abbrev-commit
```

pour vous positionner sur un commit, vous pouvez utiliser

```
git checkout <code du commit>
```

le code du commit est affiché par `git log`. Il n'a pas besoin d'être fourni
entièrement, seuls les quelques premiers carractères sont nécessaires. 

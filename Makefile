OPTIONS=-g -Wall -std=c++11 -pedantic

all: main

b.o: b.cpp b.hpp c.hpp
	g++ $(OPTIONS) -c b.cpp -o b.o

a.o: a.cpp a.hpp c.hpp
	g++ $(OPTIONS) -c a.cpp -o a.o

main.o: main.cpp a.hpp b.hpp c.hpp
	g++ $(OPTIONS) -c main.cpp -o main.o

main: main.o a.o b.o
	g++ $(OPTIONS) main.o a.o b.o -o main

clean:
	rm -f *.o main

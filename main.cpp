#include "a.hpp"
#include "b.hpp"
#include "c.hpp"

#include <iostream>

int main() {

  std::cout << a_fonction() << std::endl ;
  std::cout << b_fonction() << std::endl ;
  std::cout << c_fonction() << std::endl ;

  return 0 ;
}
